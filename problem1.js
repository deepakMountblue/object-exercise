function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys
    if(typeof obj !== 'object'){
        return([])
    }
    const allKeys=[]
    for(let i in obj){
        allKeys.push(i)
    }
    return allKeys
  }

  module.exports=keys