function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
    if(typeof obj !== 'object'){
        return([])
    }
    const allVal=[]
    for(let i in obj){
        if(typeof obj[i]!=='function'){
            allVal.push(obj[i])
        }
    }
    return allVal
  }
  
  module.exports=values