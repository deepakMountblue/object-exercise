
function mapObject(obj,cb) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
    if(typeof obj !== 'object'){
        return([])
    }
    const newObj={...obj}
    for(let i in obj){
        newObj[i]=cb(i,obj[i])
    }
    return newObj
  }
  
  module.exports=mapObject