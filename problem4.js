
function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
    if(typeof obj !== 'object'){
        return([])
    }
    const pairArr=[]
    for(let i in obj){
        pairArr.push([i,obj[i]])
    }
    return pairArr
  }
  module.exports=pairs