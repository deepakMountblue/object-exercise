
function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert
    if(typeof obj !== 'object'){
        return({})
    }
    const invertObj={}
    for(let i in obj){
        invertObj[obj[i].toString()]=i
    }
    return(invertObj)
  }

  module.exports=invert