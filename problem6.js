
function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
    if(typeof obj !== 'object'){
        return({})
    }
    for(let i in defaultProps){
        if(!obj[i]){
            if(defaultProps[i]){
                obj[i]=defaultProps[i]
            }
        }
    }
    return(obj)
  }

  module.exports=defaults